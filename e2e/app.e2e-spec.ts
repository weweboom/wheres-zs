import { WheresZSPage } from './app.po';

describe('wheres-zs App', () => {
  let page: WheresZSPage;

  beforeEach(() => {
    page = new WheresZSPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
